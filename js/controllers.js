'use strict';

var dtAppControllers = angular.module('dtControllers',['dtServices']);

// Nav Controller
dtAppControllers.controller("NavController", function($rootScope, $state, $scope, $window, SecureThing) {
  SecureThing.get({"act":"current_user","noauth":true},function(response){
    $rootScope.user = response.obj
  })

  $scope.logout = function(){
    SecureThing.get({"act":"logout"},function(response){
      relay.clear()
      $window.location.href = "/"
    })
  }
})

// Thing View Controller
.controller('BookmarkController', function ($scope, DuckDuckGo){
  $scope.relevant = [
    {"name":"Amazon","url":"www.amazon.com","favicon":"https://www.amazon.com/favicon.ico"},
    {"name":"Book Scouter","url":"www.bookscouter.com","favicon":"https://bookscouter.com/favicon.ico"},
    {"name":"Amazon","url":"www.amazon.com","favicon":"https://www.amazon.com/favicon.ico"},
    {"name":"Book Scouter","url":"www.bookscouter.com","favicon":"https://bookscouter.com/favicon.ico"},
    {"name":"Amazon","url":"www.amazon.com","favicon":"https://www.amazon.com/favicon.ico"},
    {"name":"Book Scouter","url":"www.bookscouter.com","favicon":"https://bookscouter.com/favicon.ico"},
    {"name":"Amazon","url":"www.amazon.com","favicon":"https://www.amazon.com/favicon.ico"},
    {"name":"Book Scouter","url":"www.bookscouter.com","favicon":"https://bookscouter.com/favicon.ico"},
    {"name":"Amazon","url":"www.amazon.com","favicon":"https://www.amazon.com/favicon.ico"},
    {"name":"Book Scouter","url":"www.bookscouter.com","favicon":"https://bookscouter.com/favicon.ico"},
    {"name":"Amazon","url":"www.amazon.com","favicon":"https://www.amazon.com/favicon.ico"},
    {"name":"Book Scouter","url":"www.bookscouter.com","favicon":"https://bookscouter.com/favicon.ico"}
  ]
  $scope.recent = [
    {"name":"Amazon","url":"www.amazon.com","favicon":"https://www.amazon.com/favicon.ico"},
    {"name":"Book Scouter","url":"www.bookscouter.com","favicon":"https://bookscouter.com/favicon.ico"},
    {"name":"Amazon","url":"www.amazon.com","favicon":"https://www.amazon.com/favicon.ico"},
    {"name":"Book Scouter","url":"www.bookscouter.com","favicon":"https://bookscouter.com/favicon.ico"},
    {"name":"Amazon","url":"www.amazon.com","favicon":"https://www.amazon.com/favicon.ico"},
    {"name":"Book Scouter","url":"www.bookscouter.com","favicon":"https://bookscouter.com/favicon.ico"},
    {"name":"Amazon","url":"www.amazon.com","favicon":"https://www.amazon.com/favicon.ico"},
    {"name":"Book Scouter","url":"www.bookscouter.com","favicon":"https://bookscouter.com/favicon.ico"},
    {"name":"Amazon","url":"www.amazon.com","favicon":"https://www.amazon.com/favicon.ico"},
    {"name":"Book Scouter","url":"www.bookscouter.com","favicon":"https://bookscouter.com/favicon.ico"},
    {"name":"Amazon","url":"www.amazon.com","favicon":"https://www.amazon.com/favicon.ico"},
    {"name":"Book Scouter","url":"www.bookscouter.com","favicon":"https://bookscouter.com/favicon.ico"}
  ]
  $scope.bookmarks = [
    {"name":"Amazon","url":"www.amazon.com","favicon":"https://www.amazon.com/favicon.ico"},
    {"name":"Book Scouter","url":"www.bookscouter.com","favicon":"https://bookscouter.com/favicon.ico"}
  ]

  $scope.chooseSite = function(site){
    $scope.site = site
    $scope.site_history = [
      {"url":"https://bookscouter.com/buy/0073524204-Marine-Biology","name":"Marine Biology by Castro, Peter - 0073524204, 9780073524207 | BookScouter.com"},
      {"url":"https://bookscouter.com/buy/0021362998-American-History","name":"American History by Alan Brinkley - 0021362998, 9780021362998 | BookScouter.com"},
      {"url":"https://bookscouter.com/buy/0073524204-Marine-Biology","name":"Marine Biology by Castro, Peter - 0073524204, 9780073524207 | BookScouter.com"},
      {"url":"https://bookscouter.com/buy/0021362998-American-History","name":"American History by Alan Brinkley - 0021362998, 9780021362998 | BookScouter.com"},
      {"url":"https://bookscouter.com/buy/0073524204-Marine-Biology","name":"Marine Biology by Castro, Peter - 0073524204, 9780073524207 | BookScouter.com"},
      {"url":"https://bookscouter.com/buy/0021362998-American-History","name":"American History by Alan Brinkley - 0021362998, 9780021362998 | BookScouter.com"},
      {"url":"https://bookscouter.com/buy/0073524204-Marine-Biology","name":"Marine Biology by Castro, Peter - 0073524204, 9780073524207 | BookScouter.com"},
      {"url":"https://bookscouter.com/buy/0021362998-American-History","name":"American History by Alan Brinkley - 0021362998, 9780021362998 | BookScouter.com"},
      {"url":"https://bookscouter.com/buy/0073524204-Marine-Biology","name":"Marine Biology by Castro, Peter - 0073524204, 9780073524207 | BookScouter.com"},
      {"url":"https://bookscouter.com/buy/0021362998-American-History","name":"American History by Alan Brinkley - 0021362998, 9780021362998 | BookScouter.com"},
      {"url":"https://bookscouter.com/buy/0073524204-Marine-Biology","name":"Marine Biology by Castro, Peter - 0073524204, 9780073524207 | BookScouter.com"},
      {"url":"https://bookscouter.com/buy/0021362998-American-History","name":"American History by Alan Brinkley - 0021362998, 9780021362998 | BookScouter.com"},
      {"url":"https://bookscouter.com/buy/0073524204-Marine-Biology","name":"Marine Biology by Castro, Peter - 0073524204, 9780073524207 | BookScouter.com"},
      {"url":"https://bookscouter.com/buy/0021362998-American-History","name":"American History by Alan Brinkley - 0021362998, 9780021362998 | BookScouter.com"},
      {"url":"https://bookscouter.com/buy/0073524204-Marine-Biology","name":"Marine Biology by Castro, Peter - 0073524204, 9780073524207 | BookScouter.com"},
      {"url":"https://bookscouter.com/buy/0021362998-American-History","name":"American History by Alan Brinkley - 0021362998, 9780021362998 | BookScouter.com"}
    ]
  }

  $scope.searchSite = function(site){
    var query = "site:"+site.url+" "+$scope.query;
    console.log("searching for "+query);
    DuckDuckGo.get({"q":query},function(response){
      console.log(response.obj)
    })
  }

  $(".sites-list").height($(window).height()-80);
})

// Thing List Controller
.controller('ThingListController', function($scope, SecureThing, OtherThing) {
  DTListController($scope,SecureThing,25)
  $scope.order="name" // default ordering

  //populate a select dropdown
  OtherThing.get({},function(response){
    $scope.other_things = response.obj
  })

  $scope.addOtherThing = function(){
    $scope.newItem.other_things.push({})
  }

  $scope.removeOtherThing = function(idx){
    $scope.newItem.other_things.splice(idx, 1);
  }

});

dtAppControllers.controller('LoginController', function($scope,$stateParams,Login){
  $scope.login = function(auth){
    auth.act = "authenticate"
    auth.oauth_token = $stateParams.oauth_token
    Login.save(auth,function(user){$scope.user=user})
  }
})
