'use strict';

angular.module('dtServices', ['ngResource'])
.factory("Login", function($resource) {
  return $resource(relay.url, relay.data({"end":"authentication.php"}), {})
})
.factory('DuckDuckGo', function($resource) { //http://api.duckduckgo.com/?q=DuckDuckGo&format=json&pretty=1
  return $resource(relay.url,{"api":"duckduckgo","format":"json"});
})
.factory('SecureThing', function($resource) {
  return $resource(relay.url,{"end":"secure/things.php","api":"secure"});
})
