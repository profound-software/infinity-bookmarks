'use strict';

angular.module('d3', [])
  .factory('d3Service', ['$document', '$q', '$rootScope', function($document, $q, $rootScope) {
    var d = $q.defer();
    function onScriptLoad() { // Load client in the browser
      $rootScope.$apply(function() { d.resolve(window.d3); });
    }
    // Create a script tag with d3 as the source
    // and call our onScriptLoad callback when it
    // has been loaded
    var scriptTag = $document[0].createElement('script');
    scriptTag.type = 'text/javascript';
    scriptTag.async = true;
    scriptTag.src = "http://d3js.org/d3.v3.min.js";
    scriptTag.onreadystatechange = function () {
    if (this.readyState === 'complete') { onScriptLoad(); }
  };
  scriptTag.onload = onScriptLoad;

  var s = $document[0].getElementsByTagName('body')[0];
  s.appendChild(scriptTag);

  return {
    d3: function() { return d.promise; }
  };
}]);

var dtApp = angular.module(['dtApp'], [
  'dtControllers',
  'dtServices',
  'ui.router',
  'ngAnimate',
  'ngSanitize',
  'ngTouch',
  'ui.bootstrap',
  'd3',
  'angularUtils.directives.dirPagination'
])
.run([
  '$q', '$rootScope','$state','$stateParams',
  function($q, $rootScope, $state, $stateParams) {
    // Set defaults
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
  }
])
.config(
  function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

    $urlRouterProvider
    .otherwise('/');

    $stateProvider
    .state('body', {
      abstract: true,
      url: '',
      templateUrl: 'partials/body.html',
      controller: 'NavController'
    })
    .state('index', {
      url: '/',
      templateUrl: 'partials/index.html',
      parent: 'body',
      controller: "BookmarkController"
    })
    .state('login',{
      url: '/login?oauth_token',
      templateUrl: 'partials/login.html',
      controller: 'LoginController',
      parent: 'body'
    })

    .state('thing', {
      abstract: true,
      url: '/thing',
      templateUrl: '<div ui-view />',
      parent: 'body'
    })
    .state('thing.view', {
      url: '/:id',
      templateUrl: 'partials/thing/view.html',
      controller: 'ThingViewController'
    })
    .state('thing.list', {
      url: '',
      templateUrl: 'partials/thing.list.html',
      controller: 'ThingListController'
    })

    $locationProvider.html5Mode(true);
  }
)
//pagination goodies
.config(function(paginationTemplateProvider) {
    paginationTemplateProvider.setPath('bower_components/angular-utils-pagination/dirPagination.tpl.html');
})
